export EVAHOME=$PWD/evaluationhome
export PYTHONPATH=/ATLASMLHPO/module/:/gpfs/alpine/proj-shared/csc343/zhangr/public/lib/:$EVAHOME/packages:/sw/summit/ibm-wml-ce/anaconda-base/envs/ibm-wml-ce-1.7.0-3/lib/python3.6/site-packages/:$PYTHONPATH

## CaloImage payload
export PYTHONPATH=/ATLASMLHPO/payload/CaloImageDNN/deepcalo:$PYTHONPATH
#curl -sSL https://cernbox.cern.ch/index.php/s/HfHYEsmJNWiefu3/download | tar -xzvf -; 
#python /ATLASMLHPO/payload/CaloImageDNN/run_model.py --exp_dir exp_scalars/ --data_path dataset/event100.h5 --rm_bad_reco True --zee_only True -g 0 -i /ATLASMLHPO/payload/CaloImageDNN/exp_scalars/input_example.json
python /ATLASMLHPO/payload/CaloImageDNN/run_model.py --exp_dir exp_scalars/ --data_path /gpfs/alpine/proj-shared/csc343/zhangr/public/dataset/event100.h5 --rm_bad_reco True --zee_only True -g 0 -i /ATLASMLHPO/payload/CaloImageDNN/exp_scalars/input_example.json
